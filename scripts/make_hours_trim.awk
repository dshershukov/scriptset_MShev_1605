BEGIN{
	FS="\t"
	OFS="\t"
	ORS="\r\n"
}
{
	if (NR==1){
		$1="HID\tPaper\tTimestamp";
		print $0;
		next;
	}
	
	for (i=1;i<=NF;i++){
		if ($i == "") {$i="NA";}
	}
	
	paper = substr($1,1,8);
	timestamp = substr($1,15,2) "." substr($1,13,2) "." substr($1,9,4) " " substr($1,17,2) ":00:00.000";
	$1 = $1 "\t" paper "\t" timestamp;
	print $0;
}