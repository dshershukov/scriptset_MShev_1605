BEGIN{
	FS="\t"
	OFS="\t"
	# special for Masha
	# RS="\r"
	pnf=0;
	RS="\n"
}

{
	if (FNR==1){if ($1!="Timestamp") {print FILENAME, "BROKEN HEADS";} pnf=NF; next;}
	# special for Masha
	# sub("\n","",$1)
	curstamp=substr($1,7,4) "" substr($1,4,2) "" substr($1,1,2)
	if (pnf!=NF) { print FILENAME, (FNR "broken") }
	print FILENAME, curstamp
}