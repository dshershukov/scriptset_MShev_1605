BEGIN{
	FS="\t";
	OFS="\t";
	dist=300000
	prev_stamp="TEMP"
	fstamp=12
	
	ad[1]="TEMP";
	af[1]="TEMP";
	pa=0;
	bd[1]="TEMP";
	bf[1]="TEMP";
	pb=0;
	
	fsrcd=9
	fsrcf=13
	fdestd=14
	fdestf=15
}

{
	# if (NR==1) {print $0, "lag_5min", "time_for_lag"; next}
	if ($1=="Timestamp") {print $0, "lag_5min", "time_for_lag"; next}
	if (pa==0 || $fstamp != prev_stamp) {
		ad[1]=$fsrcd
		af[1]=$fsrcf
		pa=1
		print $0, "", ""
		prev_stamp=$fstamp
		next;
	}
	
	$fdestd=""
	$fdestf=""
	for (i=1;i<=pa;i++){
		if ($fsrcf < af[i]-dist) {
			ad[i]=""
			af[i]=""
		} else {
			$fdestd=ad[i]
			$fdestf=af[i]
			break
		}
	}
	delete bd
	delete bf
	pb = 0
	for (i=1;i<=pa;i++){
		if (ad[i] != "") {
			pb++
			bd[pb]=ad[i]
			bf[pb]=af[i]
		}
	}
	delete ad
	delete af
	pa=pb
	for (i=1;i<=pb;i++){
		ad[i]=bd[i]
		af[i]=bf[i]
	}
	pa++
	ad[pa]=$fsrcd
	af[pa]=$fsrcf
	prev_stamp=$fstamp
	print $0
}