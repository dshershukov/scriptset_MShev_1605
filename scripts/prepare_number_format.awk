BEGIN{
	FS="\t"
	OFS="\t"
}
{
	#  1	FILENAME
	#  2	FNR
	#  3	NR for data
	#  4	Timestamp
	#  5	Last
	#  6	Trade Price
	#  7	Trade Volume
	#  8	Best Bid
	#  9	Bid Size
	# 10	Best Ask
	# 11	Ask Size
	# 12	Turnover
	# 13	Calc VWAP
	# 14	Flow
	# 15	Trade Flags
	
	sub(",",".",$5)
	sub(",",".",$6)
	sub(",",".",$7)
	sub(",",".",$8)
	sub(",",".",$9)
	sub(",",".",$10)
	sub(",",".",$11)
	sub(",",".",$12)
	sub(",",".",$13)
	sub(",",".",$14)
	
	print $0
}