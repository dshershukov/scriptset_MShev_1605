BEGIN {
	box[1] = "Timestamp"
	box[2] = "Last"
	box[3] = "Trade Price"
	box[4] = "Trade Volume"
	box[5] = "Best Bid"
	box[6] = "Bid Size"
	box[7] = "Best Ask"
	box[8] = "Ask Size"
	box[9] = "Turnover"
	box[10] = "Calc VWAP"
	box[11] = "Flow"
	box[12] = "Trade Flags"
	
	mask[1] = ""
	mask[2] = ""
	mask[3] = ""
	mask[4] = ""
	mask[5] = ""
	mask[6] = ""
	mask[7] = ""
	mask[8] = ""
	mask[9] = ""
	mask[10] = ""
	mask[11] = ""
	mask[12] = ""
	
	OFS = "\t"
	FS = "\t"
	RS = "\n"
}

{
	OFS = "\t"
	FS = "\t"
	
	if (/Timestamp/ == 1) {
		for (i=1; i <= NF; i++) {
			for (j = 1; j <= 12; j++) {
				if ($i == box[j]) {mask[j] = i}
			}
		}
	}
	
	print  FILENAME, sprintf("%09d", FNR), $(mask[1]), $(mask[2]), $(mask[3]), $(mask[4]), $(mask[5]), $(mask[6]), $(mask[7]), $(mask[8]), $(mask[9]), $(mask[10]), $(mask[11]), $(mask[12])
}