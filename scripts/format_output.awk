BEGIN{
	FS=" "
	OFS="\t"
}
{
	b = NF-2
	a = NF-1
	if (NF>2){
	print $a, $NF, $b
	} else {
		print $0, "FATALITY"
	}
}