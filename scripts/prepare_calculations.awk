function abs(v){ return (v < 0 ? -v : v) }

BEGIN {
	
	FS="\t"
	OFS="\t"
	
	flid = 1
	fpaper = 2
	fnr = 3
	ftimestamp = 4
	ftprice = 5
	ftvol = 6
	fbbid = 7
	fbask = 8
	fmpoint = 9
	ftprice_l = 10
	fmpoint_l = 11
	fmpoint_5min=14
	fespread_log = 16
	fqspread_log = 17
	frealized_spread = 18
	fprice_impact = 19
	
}

{
	if (NR == 1) {print $0, "espread_log", "qspread_log", "realized_spread", "price_impact"; next;}
	
	# Всё, для чего не нужны лаги
	$fespread_log = 2 * abs(log($ftprice)-log($fmpoint))
	$fqspread_log = log($fbask) - log($fbbid)
	
	$fmpoint_5min != "" ? $frealized_spread = 2 * abs(log($ftprice)-log($fmpoint_5min)) : $frealized_spread = ""
	$fmpoint_5min != "" ? $fprice_impact = 2 * abs(log($fmpoint_5min)-log($fmpoint)) : $fprice_impact = ""
	
	print $0
}