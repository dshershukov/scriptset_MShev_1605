BEGIN {
	FS="\t";
	OFS="\t";
	
	pname=2;
	tstamp=5;
	rstamp=4;
	
	# 12345678901234567890123
	# 17.09.2015 17:15:45.934
	year_beg = 7
	year_len = 4
	month_beg = 4
	month_len = 2
	day_beg = 1
	day_len = 2
	time_beg = 12
	time_len = 12
# 	hour_beg = 12
# 	hour_len = 2
# 	minute_beg = 15
# 	minute_len = 2
# 	second_beg = 18
# 	second_len = 2
# 	msecond_beg = 21
# 	msecond_len = 3
}

{
	if (NR == 1) {$2 = "Paper"; print "0_LID", $0; next;}
	
	temp_stamp = substr($tstamp,time_beg,time_len)
	gsub(":","",temp_stamp)
	gsub("\.","",temp_stamp)
	
	stamp = $pname substr($tstamp,year_beg,year_len) substr($tstamp,month_beg,month_len) substr($tstamp,day_beg,day_len) temp_stamp $rstamp
	
	print stamp, $0
}