BEGIN{
	FS="\t"
	OFS="\t"
	ftstamp=4
	fpaper=2
	fpaperdate=12
	ftime=13
}

{
	if (NR==1) { print $0, "paperdate", "special_stamp"; next;}
	# 12345678901234567890123
	# 04.01.2016 10:00:15.271
	$fpaperdate = $fpaper "" substr($ftstamp, 1, 10)
	$ftime= (substr($ftstamp,12,2) * 3600000 + substr($ftstamp,15,2) * 60000 + substr($ftstamp,18,2) * 1000+substr($ftstamp,21,3))
	print $0
}